<?php
include 'koneksi.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Web Server Aditya Yoga</title>
  </head>
  <body>
  <!-- Jumbotron -->
<div class="jumbotron jumbotron-fluid mt-5" style="background-color:cyan; background-size:100% 100%">
  <div class="container text-center">
		<font face="Arial"><h1 class="display-4 text-body">Daftar Mahasiswa Management Informatika PSDKU POLINEMA KEDIRI</h1></font>
		<img src="logo.png" width="35%">
  </div>
</div>
    <h1></h1>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<div class="container text-center mt-4" style="background-color: white ">
	<table class ="table table-bordered">
		<tr>
		<th scope="col">No</th>
		<th scope="col">Nim</th>
		<th scope="col">Nama</th>
		<th scope="col">Prodi</th>
		<th scope="col">Foto</th>
		<th scope="col">Aksi</th>
	</tr>
	<?php
	$no = 1;
	$sql = "SELECT nim ,nama, nama_prodi, concat('http://localhost/kampus/images/',photos) as url FROM mahasiswa  JOIN prodi  ON mahasiswa.id_prodi = prodi.id_prodi";
	$query = mysqli_query($koneksi, $sql);
	while ($data = mysqli_fetch_array($query)){
              ?>

            <tr>
			  <td class="text-left"><?php echo $no++; ?></td>
              <td class="text-left"><?php echo $data['nim']; ?></td>
              <td class="text-left"><?php echo $data['nama']; ?></td>
			  <td class="text-left"><?php echo $data['nama_prodi']; ?></td>
			  <td class="text-left"><img src ="<?php echo $data['url']; ?>" class="img-thumbnail" width="35%"></td>
			  <td>
				<a href="edit.php?nim=<?php echo $data['nim']; ?>" class="btn btn-success pull-center">Update </a>
				<a href="hapus.php?nim=<?php echo $data['nim']; ?>" class="btn btn-danger pull-center">Delete</a>
			  </td>
			</tr>
		 <?php } 
		 ?>
		</table>
  </body>
  </div>
  <div class= "container text-center mt-5 mb-5">
<span id='contact'></span>
	<h1><b>Tambah Mahasiswa</b></h1>
  <div class="row text-justify">
   <div class="col-lg-4">
<div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
  <div class="card-header text-center">Picture</div>
  <div class="card-body">
  <img src="pk1.png" class="card-img-top" alt="...">
  <input type="file" name="file">
  </div>
</div>
</div>
    <div class="col-7">
	<form>
  <div class="form-group">
    <label for="formGroupExampleInput" class="text-dark">Nim :</label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="nim" placeholder="">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2" class="text-dark">Nama :</label>
    <input type="text" class="form-control" id="formGroupExampleInput2" name="nama" placeholder="">
  </div>
 <div class="form-group">
    <label for="exampleFormControlTextarea1" class="text-dark">Prodi :</label>
    <select class="form-control"  name="id_prodi">
		<option value="1">Teknik Informatika</option>
		<option value="2">Mesin</option>
		<option value="3">Akuntansi</option>
	</select>
  </div>
  <button type="sumbit" name="tambah" class="btn btn-success pull-center">+ Tambah</button>
</form>
	<?php
		if(isset($_POST['tambah'])){
				$nim        = $_POST['nim'];
                $nama       = $_POST['nama'];
                $nama_prodi = $_POST['id_prodi'];
                $file       = $_POST['file'];
                $path       = "images/";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result)>0){
                    $data     = mysqli_fetch_assoc($result);
                    $id_prodi = $data['id_prodi'];
                    $sql      = "insert into mahasiswa(nim,nama,id_prodi,photos) values('$nim','$nama','$id_prodi','$file')";   
                    $result   = mysqli_query($conn,$sql);
                    if($result){
						echo '<script type="text/javascript">alert("Data berhasil disimpan")</script>';
						echo "<meta http-equiv='refresh' content='1;url=index.php?page=transaksi'>";

					}else {
						echo "<scritp>alert('Data Gagal di simpan');</script>";

				}
		mysqli_close($koneksi);
		}
	}
	?>
    </div>
  </div>
  </div>